A board designed to drive [Good Display's 4.2 inch e paper display, GDEW042T2,](https://www.good-display.com/product/226.html) with any 4-wire SPI compatible controller.
The board breaks out all interesting traces as test points. It will probably work with other Good Display products but they aren't supported.

This board is part of the [pineReader](https://www.gitlab.com/guyjeangilles/pinereader) development effort.
